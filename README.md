# Uvicorn module

[Uvicorn](https://www.uvicorn.org/) module for Patchwork framework for easy HTTP API integration with the Patchwork Node.

## Getting started

Add `patchwork.std.uvicorn_module:UvicornModule` to the list of additional modules in
you Patchwork node configuration.

Configure at least `application` option and set to your application as `[module path]:[object]`,
like for uvicorn.run() method.

There are also some additional options available:
* `listen_on`, determines IP and port to listen on, by default `127.0.0.1:8000`
* `startup_timeout`, number of seconds to wait for Uvicorn to start, default `30`
* `terminate_timeout`, number of seconds to wait for Uvicorn to terminate, default `10`
* `debug`, start Uvicorn in debug mode, by default value is same as Patchwork Node debug mode
* `reload`, if `true` runs Uvicorn with `--reload`. By default `false`
* `config`, JSON dictionary which may contain any valid Uvicorn configuration option

## Compatibility

OS:
* Linux
* *probably* any POSIX-compatible OS (not tested)
* *maybe* Windows. This library spawns and controls sub-processes which might not work correctly (not tested)

Python:
* 3.7+

Patchwork Node:
* 0.0.a4+

ASGI frameworks:
* FastAPI
* *probably* any other application which can be run using Uvicorn, especially Starlette-based