# Uvicorn module for Patchwork Node

This module integrates [Uvicorn](https://www.uvicorn.org/)  
into [Patchwork Node](https://patchwork-framework.gitlab.io/core) to add HTTP API functionality.
Currently [FastAPI](https://fastapi.tiangolo.com/) framework has been tested only, however all
ASGI applications compatible with Uvicorn should work. 

Uvicorn server is started as a Patchwork Node subprocess. This design decision have one 
big disadvantage: **code running in the API process has independent state and memory 
than main worker process**, however in most cases this should not be a problem. 

!!! warning

    Remember, than according to Patchwork Node philosophy API should
    be idempotent and read-only. In practice, it means than your API **should not** expose any other
    methods than **GET**, **HEAD** or **OPTIONS**. All data modifications should be done via
    messages passed through queues.


!!! hint

    If you need to make synchronous call to Patchwork Node you should create API Gateway
    application which post message to an appropriate queue with callback request metadata.
    When receiver completes the job it post a message to given callback with reference code.
    API Gatway should asynchronously await for this callback and return when it comes.
    
    See [our tutorial](http://patchwork.pawelpecio.eu/tutorial/api-gatway/synchronous-calls) 
    for more details.
    
 