# -*- coding: utf-8 -*-

"""Created on 04.04.2020

.. moduleauthor:: Paweł Pecio
"""
import asyncio
import multiprocessing as mp
from threading import Thread
from typing import Mapping, Dict

import uvicorn
from patchwork.node.core import Module

DEFAULT_HOST = '127.0.0.1'
DEFAULT_PORT = 8000
TERMINATE_TIMEOUT = 5
STARTUP_TIMEOUT = 30


def _main(ready_event: mp.Event, application: str, host: str, port: int,
          debug: bool = False, reload: bool = False, config: Mapping = None):

    # note: this method is run in NEW process, independent than main patchwork process

    async def callback(*args, **kwargs):
        ready_event.set()

    if config is None:
        config = {}

    config['callback_notify'] = callback

    # TODO: add logging formatting compatible with Patchwork
    #       (maybe pass logging conf from patchwork settings to uvicorn?)

    uvicorn.run(application, host=host, port=port, debug=debug, reload=reload, **config)


def _spawn_uvicorn_thread(loop: asyncio.AbstractEventLoop, future: asyncio.Future,
                          timeout: int, process_kwargs):
    """
    Uvicorn process startup watcher. This thread block until uvicorn process set process_event which means
    that uvicorn is ready. Then, thread sets local async_event and terminates.
    :param loop:
    :param future:
    :param timeout: Number of seconds to wait for event from uvicorn process to be set
    :return:
    """
    ctx = mp.get_context('spawn')
    ready_event = ctx.Event()

    api_process = ctx.Process(target=_main, args=(ready_event, ), kwargs=process_kwargs, name='fastapi-process')
    api_process.start()

    if ready_event.wait(timeout=timeout):
        loop.call_soon_threadsafe(future.set_result, api_process)
    else:
        loop.call_soon_threadsafe(future.set_exception, ChildProcessError("process startup timeout"))


class UvicornModule(Module):
    """
    This module provides FastAPI integration with Patchwork Node. The server is spawned as separate thread
    and run using uvicorn.
    """

    class Config(Module.Config):

        application: str
        listen_on: str = f"{DEFAULT_HOST}:{DEFAULT_PORT}"
        startup_timeout: int = STARTUP_TIMEOUT
        terminate_timeout: int = TERMINATE_TIMEOUT

        # work uvicorn in debug mode? if None set to worker debug flag value
        debug: bool = None

        # work uvicorn with reload flag?
        reload: bool = False

        config: Dict = {}

    _api_process: mp.Process

    @property
    def is_running(self):
        return super().is_running and self._api_process.is_alive()

    async def _start_subprocess(self):

        host, port = self.settings.listen_on.split(":")
        if not host:
            host = DEFAULT_HOST

        if not port:
            port = DEFAULT_PORT
        else:
            port = int(port)

        future = asyncio.Future()

        Thread(target=_spawn_uvicorn_thread, kwargs={
            'loop': self.app_loop,
            'future': future,
            'timeout': self.settings.startup_timeout,
            'process_kwargs': {
                'application': self.settings.application,
                'host': host,
                'port': port,
                'debug': self.settings.debug if self.settings.debug is not None else self.worker.settings.debug,
                'reload': self.settings.reload,
                'config': self.settings.config
            }
        }).start()

        try:
            self._api_process = await future
        except ChildProcessError as e:
            self._api_process.kill()
            self.logger.error(f"Can't start FastAPI subprocess: {e}")
            return False
        else:
            self.logger.info(f"FastAPI subprocess started with pid={self._api_process.pid}")
            return True

    async def _start(self):
        return await self._start_subprocess()

    async def _stop(self):
        self._api_process.terminate()
        self._api_process.join(timeout=self.settings.terminate_timeout)

        if self._api_process.is_alive():
            self.logger.error(f"FastAPI subprocess didn't compete in given terminate timeout. Killing")
            self._api_process.kill()
        else:
            self.logger.info(f"FastAPI subprocess completed")

    async def recover(self) -> bool:
        if self._api_process.is_alive():
            # recovery requested, but process is alive?
            return False

        return await self._start_subprocess() and self._api_process.is_alive()
