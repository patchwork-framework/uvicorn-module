# -*- coding: utf-8 -*-

"""Created on 06.04.2020

.. moduleauthor:: Paweł Pecio
"""
from fastapi import FastAPI

app = FastAPI()


@app.get('/')
async def check():
    return {'response': "It's working"}
