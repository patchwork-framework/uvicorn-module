# -*- coding: utf-8 -*-

"""Created on 04.04.2020

.. moduleauthor:: Paweł Pecio
"""
from typing import Mapping

import requests

from patchwork.node.testutils.cases import PatchworkTestCases
from patchwork.std.uvicorn_module import UvicornModule


class UvicornModuleTestCase(PatchworkTestCases.Module):
    module_class = UvicornModule

    def get_module_settings(self) -> Mapping:
        return {
            'application': 'tests.stubs:app'
        }

    async def _test_api_get(self):

        async with self.running_module():
            response = requests.get(f"http://{self.module.settings.listen_on}")
            self.assertEqual(response.status_code, 200)

    def test_api_get(self):
        self.loop.run_until_complete(self._test_api_get())

